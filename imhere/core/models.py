from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
	channel = models.CharField(max_length=100)
	user = models.ForeignKey(User)

class Message(models.Model):
	from_id = models.ForeignKey(User, related_name='msg_from')
	to_id = models.ManyToManyField(User, related_name='msg_to', blank=True)
	body = models.CharField(max_length=140)
	loc_name = models.CharField(max_length=100, blank=True)
	lat = models.CharField(max_length=20, blank=True)
	longt = models.CharField(max_length=20, blank=True)
	sent_ts = models.DateTimeField(blank=True, null=True)
	read_ts = models.DateTimeField(blank=True, null=True) 
	radius = models.IntegerField(blank=True, null=True)
	parent_id = models.ForeignKey('Message', blank=True, null=True)
	read = models.IntegerField()
