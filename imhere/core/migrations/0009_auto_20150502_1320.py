# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_profile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='ts',
        ),
        migrations.AddField(
            model_name='message',
            name='read_ts',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='message',
            name='sent_ts',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
