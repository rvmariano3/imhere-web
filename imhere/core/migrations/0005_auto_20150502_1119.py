# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_message_from_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='message',
            old_name='sent',
            new_name='read',
        ),
        migrations.RenameField(
            model_name='note',
            old_name='sent',
            new_name='read',
        ),
    ]
