# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20150502_1152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='to_id',
            field=models.ManyToManyField(related_name='msg_to', to=settings.AUTH_USER_MODEL, blank=True),
        ),
    ]
