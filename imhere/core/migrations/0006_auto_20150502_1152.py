# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0005_auto_20150502_1119'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='note',
            name='from_id',
        ),
        migrations.RemoveField(
            model_name='note',
            name='to_ids',
        ),
        migrations.RemoveField(
            model_name='message',
            name='from_id',
        ),
        migrations.AddField(
            model_name='message',
            name='to_id',
            field=models.ManyToManyField(related_name='msg_to', null=True, to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='parent_id',
            field=models.ForeignKey(blank=True, to='core.Message', null=True),
        ),
        migrations.DeleteModel(
            name='Note',
        ),
    ]
