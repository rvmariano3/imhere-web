# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('body', models.CharField(max_length=140)),
                ('loc_name', models.CharField(max_length=100, blank=True)),
                ('lat', models.CharField(max_length=20, blank=True)),
                ('longt', models.CharField(max_length=20, blank=True)),
                ('ts', models.DateTimeField(blank=True)),
                ('radius', models.IntegerField(null=True, blank=True)),
                ('sent', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('body', models.CharField(max_length=140)),
                ('loc_name', models.CharField(max_length=100)),
                ('lat', models.CharField(max_length=20)),
                ('longt', models.CharField(max_length=20)),
                ('ts', models.DateTimeField()),
                ('radius', models.IntegerField()),
                ('sent', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.CharField(max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='note',
            name='from_id',
            field=models.ForeignKey(related_name='note_from', to='core.User'),
        ),
        migrations.AddField(
            model_name='note',
            name='to_ids',
            field=models.ManyToManyField(related_name='note_to', to='core.User'),
        ),
        migrations.AddField(
            model_name='message',
            name='from_id',
            field=models.ForeignKey(related_name='msg_from', to='core.User'),
        ),
        migrations.AddField(
            model_name='message',
            name='parent_id',
            field=models.ForeignKey(blank=True, to='core.Note', null=True),
        ),
        migrations.AddField(
            model_name='message',
            name='to_ids',
            field=models.ManyToManyField(related_name='msg_to', to='core.User'),
        ),
    ]
