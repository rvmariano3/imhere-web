# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0009_auto_20150502_1320'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='from_id',
            field=models.ForeignKey(related_name='msg_from', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
