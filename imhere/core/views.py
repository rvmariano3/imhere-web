from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt  
from django.contrib.auth.models import User
from django.conf import settings

from datetime import datetime
import simplejson as json
from haversine import haversine
import logging
from Pubnub import Pubnub

from imhere.core.models import Profile, Message

log = logging.getLogger(__name__)

pubnub = Pubnub(publish_key=settings.PUBNUB_PUBLISH_KEY, subscribe_key=settings.PUBNUB_SUBSCRIBE_KEY)

def format_datetime(dt):
	if not dt:
		return None
	return dt.strftime('%Y-%m-%d %H:%M:%S')

@csrf_exempt
def heya(request):
	log.info(request.body)
	return JsonResponse({'peeps':['aci', 'tin', 'kido']})


@csrf_exempt
def register(request):
	pl = json.loads(request.body)
	user = User.objects.create_user(username=pl['uid'], password='a')
	user.save()

	pf = Profile.objects.create(user=user, channel=pl['channel'])
	pf.save()

	return JsonResponse({'code': 0})


@csrf_exempt
def send(request):
	pl = json.loads(request.body)
	from_user = User.objects.get(username=pl['from'])
	msg = Message.objects.create(
			from_id=from_user,
			body=pl['message'],
			loc_name=pl['loc_name'],
			lat=pl['lat'],
			longt=pl['long'],
			sent_ts=pl['send_timestamp'],
			radius=pl['radius'],
			read=0)
	msg.save()
	for to_id in pl['to']:
		msg.to_id.add(User.objects.get(username=to_id))
	msg.save()
	return JsonResponse({'code': 0, 'id': msg.pk})


def callback(message):
	print(message)


@csrf_exempt
def reply(request):
	pl = json.loads(request.body)
	from_user = User.objects.get(username=pl['from'])
	parent_note = Message.objects.get(pk=pl['parent_id'])
	msg = Message.objects.create(
			from_id=from_user,
			body=pl['message'],
			sent_ts=pl['send_timestamp'],
			parent_id=parent_note,
			read=0)
	msg.save()

	# Push message to owner
	#profile_set.all()[0] is a bad hack
	pubnub.publish(from_user.profile_set.all()[0].channel, msg.body, callback=callback, error=callback)
	# Push message to recipients
	recipients = parent_note.to_id.all()
	for rec in recipients:
		pubnub.publish(rec.profile_set.all()[0].channel, msg.body, callback=callback, error=callback)

	return JsonResponse({'code': 0, 'id': msg.pk})


@csrf_exempt
def fetch_unread(request):
	return fetch(request, [0, 1, 2])


@csrf_exempt
def fetch_all(request):
	return fetch(request, [1, 2])


@csrf_exempt
def fetch(request, sent):
	pl = json.loads(request.body)
	to_user = User.objects.get(username=pl['uid'])
	res = {"notes": [], "replies": []}

	notes = Message.objects.filter(to_id__pk=to_user.pk)
	notes_fd  = notes.filter(read__in=sent)
	for note in notes_fd:
		note.read = 2
		note.read_ts = datetime.now()
		res["notes"].append({
			"id": note.pk,
			"from": note.from_id.username,
			"to": [x.username for x in note.to_id.all()],
			"message": note.body,
			"lat": note.lat,
			"long": note.longt,
			"send_timestamp": format_datetime(note.sent_ts),
			"rcv_timestamp": format_datetime(note.read_ts),
			"radius": note.radius,
			"loc_name": note.loc_name
			})
		note.save()

	note_pks = [x.pk for x in notes]
	replies = Message.objects.filter(parent_id__in=note_pks)
	replies_fd  = replies.filter(read__in=sent)
	for reply in replies_fd:
		reply.read = 2
		reply.read_ts = datetime.now()
		res["replies"].append({
			"id": reply.pk,
			"from": reply.from_id.username,
			"message": reply.body,
			"send_timestamp": format_datetime(reply.sent_ts),
			"rcv_timestamp": format_datetime(reply.read_ts),
			})
		reply.save()

	return JsonResponse(res)


@csrf_exempt
def get_replies(request):
	pl = json.loads(request.body)
	to_user = User.objects.get(username=pl['uid'])
	res = {"replies": []}

	replies = Message.objects.filter(parent_id=int(pl['parent_id']))
	for reply in replies:
		reply.read = 2
		reply.read_ts = datetime.now()
		res["replies"].append({
			"id": reply.pk,
			"from": reply.from_id.username,
			"message": reply.body,
			"send_timestamp": format_datetime(reply.sent_ts),
			"rcv_timestamp": format_datetime(reply.read_ts),
			})
		reply.save()

	return JsonResponse(res)


@csrf_exempt
def loc_ping(request):
	pl = json.loads(request.body)
	im_at = (float(pl['lat']), float(pl['long']))

	msgs = Message.objects.filter(to_id__username=pl['uid'])
	user = User.objects.get(username=pl['uid'])
	for msg in msgs:
		msg_at = (float(msg.lat), float(msg.longt))
		if haversine(im_at, msg_at) < msg.radius and msg.read == 0:
			# push message to the recipient
			msg.read = 1
			msg.save()
			pubnub.publish(user.profile_set.all()[0].channel, msg.body, callback=callback, error=callback)
	return JsonResponse({'code': 0})


@csrf_exempt
def read_receipt(request):
	pl = json.loads(request.body)
	msg = Message.objects.get(pk=pl['msg_id'])
	msg.read = 2
	msg.read_ts = datetime.now()
	msg.save()

	# Push read notif to sender?

	return JsonResponse({'code': 0})


