from django.conf.urls import include, url, patterns
from imhere.core import views

urlpatterns = patterns(
	'',
    url(r'^heya/$', views.heya),

    url(r'^register/$', views.register),
    url(r'^send/$', views.send),
    url(r'^reply/$', views.reply),

	url(r'^fetch/$', views.fetch_unread),
    url(r'^fetchall/$', views.fetch_all),
    url(r'^note/$', views.get_replies),

    url(r'^imread/$', views.read_receipt),
    url(r'^loc_ping/$', views.loc_ping),

)
